const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackSchema = new Schema({
  title: String,
  album: {
    type: Schema.Types.ObjectId,
    ref: 'Album',
    required: true
  },
  lengthTrack: String,
  isPublished: {
    type: Boolean,
    required: true,
    default: false
  },
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;