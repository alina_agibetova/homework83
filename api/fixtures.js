const mongoose = require('mongoose');
const config = require('./config');
const Album = require('./models/Album');
const Artist = require('./models/Artist');
const Track = require('./models/Track');
const User = require('./models/User');
const TrackHistory = require('./models/TrackHistory');
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@track.com',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'user',
    avatar: 'no_image_available.jpg'
  }, {
    email: 'admin@track.com',
    password: '123',
    token: nanoid(),
    role: 'admin',
    displayName: 'admin',
    avatar: 'no_image_available.jpg'
  })

  const [JloFirst, BeyonceSecond] = await Artist.create({
    name: 'Jlo',
    image: 'jlo.jpg',
    information: 'R&B singer',
    isPublished: true
  }, {
    name: 'Beyonce',
    image: 'beyonce.jpeg',
    information: 'R&B singer',
    isPublished: false
  });

  const [JLo, BDay] = await Album.create({
    artist: JloFirst,
    name: 'JLo of the Block',
    year: '2010',
    image: 'jlo-album.jpeg',
    isPublished: true
  }, {
    artist: BeyonceSecond,
    name: 'BDay',
    year: '2012',
    image: 'beyonce-album.jpeg',
    isPublished: false
  });

  const [track1, track2] = await Track.create({
    album: JLo,
    title: 'marry me',
    lengthTrack: '3.16',
    isPublished: true
  }, {
    album: BDay,
    title: 'halo',
    lengthTrack: '4.16',
    isPublished: false
  });

  await TrackHistory.create({
    user: user,
    track: track1,
    datetime: new Date().toISOString()
  },{
    user: admin,
    track: track2,
    datetime: new Date().toISOString()
    })

  await mongoose.connection.close();
}

run().catch(e => console.error(e))