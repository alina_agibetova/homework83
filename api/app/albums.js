const express = require('express');
const path = require('path');
const multer = require('multer');
const config = require('../config');
const {nanoid} = require('nanoid');
const Album = require('../models/Album');
const auth = require('../middleware/auth');
const permit = require('../permit');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },

  filename: (req, file, cb)=>{
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', auth, async (req, res, next) => {
  try{
    if (req.query.artist) {
      const albumsByArtist = await Album.find({artist: {_id:req.query.artist}}).populate('artist', 'name');
      return res.send(albumsByArtist)
    }

    const albums = await Album.find().populate('artist', 'name');
    return res.send(albums);
  }catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try{
    const album = await Album.findById(req.params.id);

    if (!album){
      return res.status(404).send({message: 'Not found'});
    }

    if (req.user.role === 'admin'){
      req.body.isPublished = true;
    }

    return res.send(album);
  } catch (e){
    next(e);
  }
});

router.post('/', auth, permit('user', 'admin'), upload.single('image'), async (req, res, next) => {
  try{
    if (!req.body.name || !req.body.year){
      return res.status(400).send({message: 'Name and year are required'})
    }

    const albumData = {
      name: req.body.name,
      artist: req.body.artist,
      year: req.body.year,
      image: null,
      isPublished: false
    };

    if (req.file){
      albumData.image = req.file.filename;
    }

    const album = new Album(albumData);
    await album.save();
    return res.send({message: 'Created new album', id: album._id})
  }catch (e) {
    next(e);
  }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
  try{
    const publish = await Album.updateOne({_id: req.params.id}, {isPublished: req.body.isPublished});
    return res.send({message: 'Published', publish});
  } catch (e) {
    next(e)
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res, next) => {
  try{
    if (req.params.id){
      const album = await Album.deleteOne({_id: req.params.id});

      return res.send(album);
    }
  } catch (e){
    next(e);
  }
});

module.exports = router;