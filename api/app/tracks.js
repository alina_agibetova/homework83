const express = require('express');
const Track = require('../models/Track');
const mongoose = require("mongoose");
const Album = require("../models/Album");
const auth = require("../middleware/auth");
const permit = require("../permit");
const TrackHistory = require("../models/TrackHistory");

const router = express.Router();

router.get('/', async (req, res, next) => {
  try{
    const query = {};
    let artistsTracks = [];

    if (req.query.album){
      query.album = {_id: req.query.album};
    }

    if (req.query.artist) {
      const albums = await Album.find({artist: {_id: req.query.artist}});
      for (let album of albums) {
        artistsTracks= await Track.find({album: {_id: album._id}});
      }
      return res.send(artistsTracks);
    }
    const tracks = await Track.find(query).populate('album', 'name');
    return res.send(tracks);
  }catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    return next(e);
  }
});

router.post('/', auth, permit('user', 'admin'), async (req, res, next) => {
  try {
    const trackData = {
      title: req.body.title,
      album: req.body.album,
      lengthTrack: req.body.lengthTrack,
      isPublished: false
    }

    const track = new Track(trackData);
    await track.save();
    return res.send(track);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
  }
  return next(e);
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
  try{
    const publish = await Track.updateOne({_id: req.params.id}, {isPublished: req.body.isPublished});
    return res.send({message: 'Published', publish});
  } catch (e) {
    next(e)
  }
});

router.delete('/:id', auth, permit('admin') , async (req, res, next) => {
  try{
    if (req.params.id){
      const track = await Track.deleteOne({_id: req.params.id});
      return res.send(track);
    }
  } catch (e){
    next(e);
  }
});

module.exports = router;