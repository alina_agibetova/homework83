const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const path = require('path');
const config = require('../config');
const Artist = require('../models/Artist');
const permit = require('../permit');
const auth = require("../middleware/auth");
const Album = require("../models/Album");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },

  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const query = {};
    if (req.query.filter === 'image'){
      query.image = {$ne: null};
    }
    const artists = await Artist.find(query);
    return res.send(artists);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, permit('user', 'admin'), upload.single('image'), async (req, res, next) => {
  try{
    if (!req.body.name || !req.body.information){
      return res.status(400).send({message: 'Name and information are required'})
    }

    const artistData = {
      name: req.body.name,
      image: null,
      information: req.body.information,
      isPublished: false
    };
    if (req.file){
      artistData.image = req.file.filename;
    }

    const artist = new Artist(artistData);
    await artist.save();
    return res.send({message: 'Created new artist', id: artist._id});
  }catch (e) {
    next(e);
  }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
  try{
    const publish = await Artist.updateOne({_id: req.params.id}, {isPublished: req.body.isPublished});
    return res.send({message: 'Published', publish});
  } catch (e) {
    next(e)
  }
});

router.delete('/:id', auth, permit('admin'),  async (req, res, next) => {
  try{
    if (req.params.id){
      const artist = await Artist.deleteOne({_id: req.params.id});
      return res.send(artist);
    }
  } catch (e){
    next(e);
  }
});

module.exports = router;