const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const auth = require('../middleware/auth');
const mongoose = require('mongoose');
const permit = require("../permit");
const Album = require("../models/Album");
const router = express.Router();

router.post('/', auth, async (req, res , next) => {
  try{
    const trackHistoryData = {
      user: req.user._id,
      track: req.body.track,
      datetime: new Date().toISOString(),
      isPublished: false
    }

    const trackHis = new TrackHistory(trackHistoryData);
    await trackHis.save();
    return res.send(trackHis);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    return next(e);
  }

});

router.get('/', auth,  async (req, res , next) => {
  try{
    const trackHistories = await TrackHistory.find({user: req.user._id}).sort({datetime: -1}).populate('track', 'title');

    return res.send(trackHistories);
  }catch (e) {
    next(e);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res, next) => {
  try{
    if (req.params.id){
      const trackHistory = await TrackHistory.deleteOne({_id: req.params.id});
      return res.send(trackHistory);
    }
  } catch (e){
    next(e);
  }
});


module.exports = router;