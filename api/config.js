const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  mongo: {
    db: 'mongodb://localhost/homework83',
    options: {useNewUrlParser: true},
  },
  facebook: {
    appId: '479511753654437',
    appSecret: 'c3120469da7fffcce404773a670271b0'
  }
}

