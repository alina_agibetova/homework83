import { TrackState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createTrackFailure,
  createTrackPublishRequest,
  createTrackRequest,
  createTrackSuccess,
  deleteTrackFailure,
  deleteTrackRequest,
  deleteTrackSuccess,
  fetchTracksFailure,
  fetchTracksRequest,
  fetchTracksSuccess
} from './tracks.actions';

const initialState: TrackState = {
  tracks: [],
  track: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  publishLoading: false
}

export const tracksReducer = createReducer(
  initialState,
  on(fetchTracksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTracksSuccess, (state, {tracks}) =>
    ({...state, fetchLoading: false, tracks})),

  on(fetchTracksFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),


  on(createTrackRequest, state => ({...state, createLoading: true})),

  on(createTrackSuccess, state => ({...state, createLoading: false})),

  on(createTrackFailure, (state, {error}) =>
    ({...state, createLoading: false, createError: error})),


  on(deleteTrackRequest, state => ({...state, fetchLoading: true})),
  on(deleteTrackSuccess, (state, {track}) =>
    ({...state, fetchLoading: false, track})),

  on(deleteTrackFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),


  on(createTrackPublishRequest, state => ({...state, publishLoading: false}))

);
