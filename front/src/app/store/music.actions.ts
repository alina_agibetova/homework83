import { createAction, props } from '@ngrx/store';
import { Artist, ArtistData, ArtistPublishData } from '../models/artist.model';
import { Album, AlbumData, AlbumPublishData } from '../models/album.model';

export const fetchArtistsRequest = createAction(
  '[Artists] Fetch Request'
);

export const fetchArtistsSuccess = createAction(
  '[Artists] Fetch Success',
  props<{artists: Artist[]}>()
);

export const fetchArtistsFailure = createAction(
  '[Artists] Fetch Failure',
  props<{error: string}>()
);



export const fetchAlbumRequest = createAction(
  '[Album] Fetch Request',
  props<{artist: string}>()
);

export const fetchAlbumSuccess = createAction(
  '[Album] Fetch Success',
  props<{albums: Album[]}>()
);

export const fetchAlbumFailure = createAction(
  '[Album] Fetch Failure',
  props<{error: string}>()
);



export const fetchAlbumsRequest = createAction(
  '[Albums] Fetch Request'
);

export const fetchAlbumsSuccess = createAction(
  '[Albums] Fetch Success',
  props<{albums: Album[]}>()
);

export const fetchAlbumsFailure = createAction(
  '[Albums] Fetch Failure',
  props<{error: string}>()
);



export const createArtistRequest = createAction(
  '[Artists] Create Request',
  props<{artistData: ArtistData}>()
);
export const createArtistSuccess = createAction(
  '[Artists] Create Success'
);
export const createArtistFailure = createAction(
  '[Artists] Create Failure',
  props<{error: string}>()
);


export const createAlbumRequest = createAction(
  '[Albums] Create Request',
  props<{albumData: AlbumData}>()
);
export const createAlbumSuccess = createAction(
  '[Albums] Create Success'
);
export const createAlbumFailure = createAction(
  '[Albums] Create Failure',
  props<{error: string}>()
);


export const createAlbumPublishRequest = createAction(
  '[Album] Create Request',
  props<{albumPublishData: AlbumPublishData}>()
);
export const createAlbumPublishSuccess = createAction(
  '[Album] Create Success'
);
export const createAlbumPublishFailure = createAction(
  '[Album] Create Failure',
  props<{error: string}>()
);


export const createArtistPublishRequest = createAction(
  '[Artists] Create Request',
  props<{artistPublishData: ArtistPublishData}>()
);
export const createArtistPublishSuccess = createAction(
  '[Artists] Create Success'
);
export const createArtistPublishFailure = createAction(
  '[Artists] Create Failure',
  props<{error: string}>()
);


export const deleteAlbumRequest = createAction(
  '[Album] Delete Request',
  props<{id: string}>()
);

export const deleteAlbumSuccess = createAction(
  '[Album] Delete Success',
  props<{album: Album}>()
);

export const deleteAlbumFailure = createAction(
  '[Album] Delete Failure',
  props<{error: string}>()
);



export const deleteArtistRequest = createAction(
  '[Artist] Delete Request',
  props<{id: string}>()
);

export const deleteArtistSuccess = createAction(
  '[Artist] Delete Success',
  props<{artist: Artist}>()
);

export const deleteArtistFailure = createAction(
  '[Artist] Delete Failure',
  props<{error: string}>()
);
