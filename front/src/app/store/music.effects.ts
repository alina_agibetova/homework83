import { Injectable } from '@angular/core';
import { MusicService } from '../services/music.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createAlbumFailure,
  createAlbumPublishFailure,
  createAlbumPublishRequest,
  createAlbumPublishSuccess,
  createAlbumRequest,
  createAlbumSuccess,
  createArtistFailure,
  createArtistPublishFailure,
  createArtistPublishRequest,
  createArtistPublishSuccess,
  createArtistRequest,
  createArtistSuccess,
  deleteAlbumFailure,
  deleteAlbumRequest,
  deleteAlbumSuccess,
  deleteArtistFailure,
  deleteArtistRequest,
  deleteArtistSuccess,
  fetchAlbumFailure,
  fetchAlbumRequest,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess,
  fetchAlbumSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess
} from './music.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import {
  createTrackFailure,
  createTrackPublishFailure,
  createTrackPublishRequest,
  createTrackPublishSuccess,
  createTrackRequest,
  createTrackSuccess,
  deleteTrackFailure,
  deleteTrackRequest,
  deleteTrackSuccess,
  fetchTracksFailure,
  fetchTracksRequest,
  fetchTracksSuccess
} from './tracks.actions';
import { HelpersService } from '../services/helpers.service';


@Injectable()
export class MusicEffects{
  fetchArtists = createEffect(() => this.actions.pipe(
    ofType(fetchArtistsRequest),
    mergeMap(() => this.musicService.getArtists().pipe(
      map(artists => fetchArtistsSuccess({artists})),
      catchError(() => of(fetchArtistsFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchAlbumsById = createEffect(() => this.actions.pipe(
    ofType(fetchAlbumRequest),
    mergeMap(id => this.musicService.getAlbumId(id.artist).pipe(
      map(albums => fetchAlbumSuccess({albums})),
      catchError(() => of(fetchAlbumFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchAlbums = createEffect(() => this.actions.pipe(
    ofType(fetchAlbumsRequest),
    mergeMap(() => this.musicService.getAlbums().pipe(
      map(albums => fetchAlbumsSuccess({albums})),
      catchError(() => of(fetchAlbumsFailure({error: 'Something went wrong in albums'})))
    ))
  ));

  fetchTracks = createEffect(() => this.actions.pipe(
    ofType(fetchTracksRequest),
    mergeMap(id => this.musicService.getTrack(id.id).pipe(
        map(tracks => fetchTracksSuccess({tracks})),

        catchError(() => of(fetchTracksFailure({error: 'Something went wrong'})))
      ))
  ));

  createTrack = createEffect(() => this.actions.pipe(
    ofType(createTrackRequest),
    mergeMap(({trackData}) => this.musicService.createTrack(trackData).pipe(
      map(() => createTrackSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Create track successful')
      }),
      this.helpers.catchServerError(createTrackFailure)
    ))
  ));

  createAlbum = createEffect(() => this.actions.pipe(
    ofType(createAlbumRequest),
    mergeMap(({albumData}) => this.musicService.createAlbum(albumData).pipe(
      map(() => createAlbumSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Create album successful')
      }),
      this.helpers.catchServerError(createAlbumFailure)
    ))
  ));

  createArtist = createEffect(() => this.actions.pipe(
    ofType(createArtistRequest),
    mergeMap(({artistData}) => this.musicService.createArtist(artistData).pipe(
      map(() => createArtistSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Create artist successful')
      }),
      this.helpers.catchServerError(createArtistFailure)
    ))
  ));


  createPublishAlbum = createEffect(() => this.actions.pipe(
    ofType(createAlbumPublishRequest),
    mergeMap(({albumPublishData}) => this.musicService.albumIsPublish(albumPublishData).pipe(
      map(() => createAlbumPublishSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Published successful');
      }),
      this.helpers.catchServerError(createAlbumPublishFailure)
    ))
  ));


  createPublishArtist = createEffect(() => this.actions.pipe(
    ofType(createArtistPublishRequest),
    mergeMap(({artistPublishData}) => this.musicService.artistIsPublish(artistPublishData).pipe(
      map(() => createArtistPublishSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Published successful');
      }),
      this.helpers.catchServerError(createArtistPublishFailure)
    ))
  ));


  createPublishTrack = createEffect(() => this.actions.pipe(
    ofType(createTrackPublishRequest),
    mergeMap(({trackPublishData}) => this.musicService.trackIsPublish(trackPublishData).pipe(
      map(() => createTrackPublishSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Published successful');
      }),
      this.helpers.catchServerError(createTrackPublishFailure)
    ))
  ));


  deleteTrack = createEffect(() => this.actions.pipe(
    ofType(deleteTrackRequest),
    mergeMap(id => this.musicService.deleteTrack(id.id).pipe(
      map(track => deleteTrackSuccess({track})),
      tap(() => {
        this.helpers.openSnackbar('Delete track successful')
      }),
      this.helpers.catchServerError(deleteTrackFailure)
    ))
  ));

  deleteAlbum = createEffect(() => this.actions.pipe(
    ofType(deleteAlbumRequest),
    mergeMap(id => this.musicService.deleteAlbum(id.id).pipe(
      map(album => deleteAlbumSuccess({album})),
      tap(() => {
        this.helpers.openSnackbar('Delete album successful')
      }),
      this.helpers.catchServerError(deleteAlbumFailure)
    ))
  ));

  deleteArtist = createEffect(() => this.actions.pipe(
    ofType(deleteArtistRequest),
    mergeMap(id => this.musicService.deleteArtist(id.id).pipe(
      map(artist => deleteArtistSuccess({artist})),
      tap(() => {
        this.helpers.openSnackbar('Delete artist successful')
      }),
      this.helpers.catchServerError(deleteArtistFailure)
    ))
  ));


  constructor(private actions: Actions,
              private musicService: MusicService,
              private helpers: HelpersService) {}
}
