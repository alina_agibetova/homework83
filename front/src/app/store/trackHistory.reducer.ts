import { TrackHistoryState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createTrackHistoryFailure,
  createTrackHistoryRequest,
  createTrackHistorySuccess, deleteTrackHistoryFailure, deleteTrackHistoryRequest, deleteTrackHistorySuccess,
  fetchTrackHistoriesFailure,
  fetchTrackHistoriesRequest,
  fetchTrackHistoriesSuccess
} from './trackHistory.actions';

const initialState: TrackHistoryState = {
  trackHistories: [],
  trackHistory: null,
  createLoading: false,
  createError: null,
  fetchLoading: false,
  fetchError: null,
}

export const trackHistoriesReducer = createReducer(
  initialState,

  on(createTrackHistoryRequest, state => ({...state, createLoading: true})),

  on(createTrackHistorySuccess, state => ({...state, createLoading: false})),

  on(createTrackHistoryFailure, (state, {error}) =>
    ({...state, createLoading: false, createError: error})),

  on(fetchTrackHistoriesRequest, state => ({...state, fetchLoading: true})),
  on(fetchTrackHistoriesSuccess, (state, {trackHistories}) =>
    ({...state, fetchLoading: false, trackHistories})),

  on(fetchTrackHistoriesFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),


  on(deleteTrackHistoryRequest, state => ({...state, fetchLoading: true})),
  on(deleteTrackHistorySuccess, (state, {trackHistory}) =>
    ({...state, fetchLoading: false, trackHistory})),

  on(deleteTrackHistoryFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),
);
