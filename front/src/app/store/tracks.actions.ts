import { createAction, props } from '@ngrx/store';
import { Track, TrackData, TrackPublishData } from '../models/track.model';

export const fetchTracksRequest = createAction(
  '[Tracks] Fetch Request',
  props<{id: string}>()
);

export const fetchTracksSuccess = createAction(
  '[Tracks] Fetch Success',
  props<{tracks: Track[]}>()
);

export const fetchTracksFailure = createAction(
  '[Track] Fetch Failure',
  props<{error: string}>()
);


export const createTrackRequest = createAction(
  '[Tracks] Create Request',
  props<{trackData: TrackData}>()
);
export const createTrackSuccess = createAction(
  '[Tracks] Create Success'
);
export const createTrackFailure = createAction(
  '[Tracks] Create Failure',
  props<{error: string}>()
);


export const deleteTrackRequest = createAction(
  '[Track] Delete Request',
  props<{id: string}>()
);

export const deleteTrackSuccess = createAction(
  '[Track] Delete Success',
  props<{track: Track}>()
);

export const deleteTrackFailure = createAction(
  '[Track] Delete Failure',
  props<{error: string}>()
);


export const createTrackPublishRequest = createAction(
  '[Track] Create Request',
  props<{trackPublishData: TrackPublishData}>()
);
export const createTrackPublishSuccess = createAction(
  '[Track] Create Success'
);
export const createTrackPublishFailure = createAction(
  '[Track] Create Failure',
  props<{error: string}>()
);

