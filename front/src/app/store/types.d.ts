import { Artist } from '../models/artist.model';
import { Album } from '../models/album.model';
import { LoginError, RegisterError, User } from '../models/user.model';
import { Track } from '../models/track.model';
import { TrackHistory } from '../models/trackHistory.model';

export type ArtistState = {
  artists: Artist[],
  artist: Artist | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  publishLoading: boolean
};

export type AlbumState = {
  albums: Album[];
  album: Album | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  publishLoading: boolean
}

export type TrackState = {
  tracks: Track[],
  track: Track | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  publishLoading: boolean
}

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError
}

export type TrackHistoryState = {
  trackHistories: TrackHistory[],
  trackHistory: TrackHistory | null,
  createLoading: boolean,
  createError: null | string,
  fetchLoading: boolean,
  fetchError: null | string,
}

export type AppState = {
  artists: ArtistState,
  albums: AlbumState,
  users: UsersState,
  tracks: TrackState,
  trackHistories: TrackHistoryState
}
