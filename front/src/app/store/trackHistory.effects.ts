import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MusicService } from '../services/music.service';
import { registerUserFailure } from './users.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import {
  createTrackHistoryRequest,
  createTrackHistorySuccess,
  deleteTrackHistoryFailure,
  deleteTrackHistoryRequest,
  deleteTrackHistorySuccess,
  fetchTrackHistoriesRequest,
  fetchTrackHistoriesSuccess
} from './trackHistory.actions';
import { HelpersService } from '../services/helpers.service';
import { Router } from '@angular/router';
import { fetchTracksFailure } from './tracks.actions';

@Injectable()
export class TrackHistoryEffects{
  constructor(private actions: Actions,
              private musicService: MusicService,
              private helpers: HelpersService,
              private router: Router) {
  }

  createTrackHistories = createEffect(() => this.actions.pipe(
    ofType(createTrackHistoryRequest),
    mergeMap(({trackHistoryData}) => this.musicService.createTrackHistory(trackHistoryData).pipe(
      map(()=> createTrackHistorySuccess()),
      tap(() => {
        this.helpers.openSnackbar('Successful')
      }),
      this.helpers.catchServerError(registerUserFailure)
    ))
  ));

  fetchTrackHistories = createEffect(() => this.actions.pipe(
    ofType(fetchTrackHistoriesRequest),
    mergeMap(() => this.musicService.getTrackHistory().pipe(
      map(trackHistories => fetchTrackHistoriesSuccess({trackHistories})),

      catchError(() => of(fetchTracksFailure({error: 'Something went wrong'})))
    ))
  ));


  deleteTrackHistory = createEffect(() => this.actions.pipe(
    ofType(deleteTrackHistoryRequest),
    mergeMap(id => this.musicService.deleteTrackHistory(id.id).pipe(
      map(trackHistory => deleteTrackHistorySuccess({trackHistory})),
        tap(() => {
          this.helpers.openSnackbar('Delete track-history successful')
        }),
        this.helpers.catchServerError(deleteTrackHistoryFailure)
      ))
  ));

}
