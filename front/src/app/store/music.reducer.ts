import { createReducer, on } from '@ngrx/store';
import { AlbumState, ArtistState } from './types';
import {
  createAlbumFailure, createAlbumPublishRequest,
  createAlbumRequest,
  createAlbumSuccess,
  createArtistFailure, createArtistPublishRequest,
  createArtistRequest,
  createArtistSuccess, fetchAlbumFailure, fetchAlbumRequest,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess, fetchAlbumSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess
} from './music.actions';

const initialState: ArtistState = {
  artists: [],
  artist: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  publishLoading: false,
};

const initialStateAlbums: AlbumState = {
  albums: [],
  album: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  publishLoading: false,
}

export const artistsReducer = createReducer(
  initialState,
  on(fetchArtistsRequest, state => ({...state, fetchLoading: true})),
  on(fetchArtistsSuccess, (state, {artists}) =>
    ({...state, fetchLoading: false, artists})),

  on(fetchArtistsFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),

  on(createArtistRequest, state => ({...state, createLoading: true})),

  on(createArtistSuccess, state => ({...state, createLoading: false})),

  on(createArtistFailure, (state, {error}) =>
    ({...state, createLoading: false, createError: error})),

  on(createArtistPublishRequest, state => ({...state, publishLoading: false}))
);

export const albumsReducer = createReducer(
  initialStateAlbums,
  on(fetchAlbumsRequest, state => ({...state, fetchLoading: true})),
  on(fetchAlbumsSuccess, (state, {albums }) =>
    ({...state, fetchLoading: false, albums})),

  on(fetchAlbumsFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),


  on(fetchAlbumRequest, state => ({...state, fetchLoading: true})),
  on(fetchAlbumSuccess, (state, {albums }) =>
    ({...state, fetchLoading: false, albums})),

  on(fetchAlbumFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),


  on(createAlbumRequest, state => ({...state, createLoading: true})),

  on(createAlbumSuccess, state => ({...state, createLoading: false})),

  on(createAlbumFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
  on(createAlbumPublishRequest, state => ({...state, publishLoading: false}))
);

