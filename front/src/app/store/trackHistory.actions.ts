import { createAction, props } from '@ngrx/store';
import { TrackHistory } from '../models/trackHistory.model';

export const createTrackHistoryRequest = createAction(
  '[TracksHistory] Create Request',
  props<{trackHistoryData: string}>());
export const createTrackHistorySuccess = createAction(
  '[TracksHistory] Create Success'
);
export const createTrackHistoryFailure = createAction(
  '[TracksHistory] Create Failure', props<{error: string}>());


export const fetchTrackHistoriesRequest = createAction(
  '[TrackHistory] Fetch Request',
);

export const fetchTrackHistoriesSuccess = createAction(
  '[TrackHistory] Fetch Success',
  props<{trackHistories: TrackHistory[]}>()
);

export const fetchTrackHistoriesFailure = createAction(
  '[TrackHistory] Fetch Failure',
  props<{error: string}>()
);


export const deleteTrackHistoryRequest = createAction(
  '[TrackHistory] Delete Request',
  props<{id: string}>()
);

export const deleteTrackHistorySuccess = createAction(
  '[TrackHistory] Delete Success',
  props<{trackHistory: TrackHistory}>()
);

export const deleteTrackHistoryFailure = createAction(
  '[TrackHistory] Delete Failure',
  props<{error: string}>()
);
