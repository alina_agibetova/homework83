import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArtistsComponent } from './pages/artists/artists.component';
import { AlbumsComponent } from './pages/albums/albums.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { AlbumComponent } from './pages/albums/album/album.component';
import { TrackHistoryComponent } from './pages/track-history/track-history.component';
import { RoleGuardService } from './services/role-guard.service';
import { NewArtistComponent } from './pages/new-artist/new-artist.component';
import { NewAlbumComponent } from './pages/new-album/new-album.component';
import { NewTrackComponent } from './pages/new-track/new-track.component';
import { TracksComponent } from './pages/tracks/tracks.component';

const routes: Routes = [
  {path: '', component: ArtistsComponent},
  {path: 'albums', component: AlbumsComponent},
  {
    path: 'album/:id',
    component: AlbumComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
  {
    path: ':id/:artist/:name',
    component: TracksComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'new-artist',
    component: NewArtistComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['user', 'admin']}
  },
  {
    path: 'new-album',
    component: NewAlbumComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['user', 'admin']}
  },
  {
    path: 'new-track',
    component: NewTrackComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['user', 'admin']}
  },
  {
    path: 'track-history',
    component: TrackHistoryComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['user', 'admin']}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
