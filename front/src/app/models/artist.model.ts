export class Artist {
  constructor(
    public _id: string,
    public name: string,
    public image: string,
    public information: string,
    public isPublished: boolean
  ) {}
}

export interface ArtistData {
  [key: string]: any;
  name: string;
  image: File | null;
  information: string,
  isPublished: boolean
}

export interface ApiArtistData {
  _id: string,
  name: string,
  image: string,
  information: string,
  isPublished: boolean
}

export interface ArtistPublishData {
  id: string,
  isPublished: boolean
}
