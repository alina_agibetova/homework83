export class Album {
  constructor(
    public _id: string,
    public name: string,
    public artist: {
      _id: string,
      name: string
    },
    public year: string,
    public image: string,
    public isPublished: boolean
  ) {}
}

export interface AlbumData {
  [key: string]: any;
  name: string;
  artist: {
    _id: string,
    name: string
  },
  year: string,
  image: File | null,
  isPublished: boolean
}

export interface ApiAlbumData {
  _id: string,
  name: string,
  artist: {
    _id: string,
    name: string
  },
  year: string,
  image: string,
  isPublished: boolean
}

export interface AlbumPublishData {
  id: string,
  isPublished: boolean
}
