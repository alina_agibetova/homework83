export class Track {
  constructor(
    public _id: string,
    public title: string,
    public album: string,
    public lengthTrack: string,
    public isPublished: boolean
  ) {}
}

export interface TrackData {
  [key: string]: any,
  _id: string,
  title: string,
  album: string,
  lengthTrack: string,
  isPublished: boolean
}

export interface ApiTrackData {
  _id: string,
  title: string,
  album: string,
  lengthTrack: string,
  isPublished: boolean
}

export interface TrackPublishData {
  id: string,
  isPublished: boolean
}
