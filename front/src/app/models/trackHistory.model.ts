export class TrackHistory{
  constructor(
    public _id: string,
    public user: string,
    public track: {
      _id: string,
      title: string,
      album: string,
    },
    public datetime: string
  ) {}
}

export interface TrackHistoryData {
  user: string,
  track: string,
  token: string,
}

export interface ApiTrackHistoryData {
  _id: string,
  user: string,
  track: {
    _id: string,
    title: string,
    album: string
  },
  token: string,
  datetime: string
}
