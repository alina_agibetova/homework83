import { NgModule } from '@angular/core';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { usersReducer } from './store/users.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './store/users.effects';
import { albumsReducer, artistsReducer } from './store/music.reducer';
import { MusicEffects } from './store/music.effects';
import { localStorageSync } from 'ngrx-store-localstorage';
import { tracksReducer } from './store/tracks.reducer';
import { trackHistoriesReducer } from './store/trackHistory.reducer';
import { TrackHistoryEffects } from './store/trackHistory.effects';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];
const reducers = {
  artists: artistsReducer, users: usersReducer, albums: albumsReducer, tracks: tracksReducer, trackHistories: trackHistoriesReducer
}

const effects = [MusicEffects, UsersEffects, TrackHistoryEffects];

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ],
  exports: [StoreModule, EffectsModule]
})
export class AppStoreModule{}
