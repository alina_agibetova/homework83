import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Album, AlbumPublishData } from '../../../models/album.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { ActivatedRoute } from '@angular/router';
import { createAlbumPublishRequest, deleteAlbumRequest, fetchAlbumRequest } from '../../../store/music.actions';
import { User } from '../../../models/user.model';
import { Track } from '../../../models/track.model';
import { TrackHistory } from '../../../models/trackHistory.model';
import { fetchTracksRequest } from '../../../store/tracks.actions';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.sass']
})
export class AlbumComponent implements OnInit {
  user: Observable<User | null>;
  albums: Observable<Album[]>;
  deleteAlbum: Observable<Album | null>
  loading: Observable<boolean>;
  error: Observable<null | string>;
  userSub!: Subscription;
  tracks: Observable<Track[]>;
  trackHistory: Observable<TrackHistory[]>;
  publishAlbum: Observable<boolean>

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.albums = store.select( state => state.albums.albums);
    this.loading = store.select(state => state.albums.fetchLoading);
    this.error = store.select(state => state.albums.fetchError);
    this.user = store.select(state => state.users.user);
    this.tracks = store.select(state => state.tracks.tracks);
    this.trackHistory = store.select(state => state.trackHistories.trackHistories);
    this.deleteAlbum = store.select(state => state.albums.album);
    this.publishAlbum = store.select(state => state.albums.publishLoading)

  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchAlbumRequest({artist: params['id']}));
    });
  }

  onPublish(id: string){
    const publish: AlbumPublishData = {
      id: id,
      isPublished: true
    }
    this.store.dispatch(createAlbumPublishRequest({albumPublishData: publish}))
  }

  onClick(id: string){
    this.store.dispatch(fetchTracksRequest({id: id}))
  }


  onDelete(_id: string) {
    this.store.dispatch(deleteAlbumRequest({id: _id}))
  }
}
