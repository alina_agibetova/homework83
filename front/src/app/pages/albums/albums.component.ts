import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Album } from '../../models/album.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { fetchAlbumsRequest } from '../../store/music.actions';
import { ActivatedRoute } from '@angular/router';
import { MusicService } from '../../services/music.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.sass']
})
export class AlbumsComponent implements OnInit {
  albums: Observable<Album[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>

  constructor(private store: Store<AppState>, private route: ActivatedRoute, private musicService: MusicService) {
    this.albums = store.select( state => state.albums.albums);
    this.loading = store.select(state => state.albums.fetchLoading);
    this.error = store.select(state => state.albums.fetchError)
  }

  ngOnInit(): void {
    this.store.dispatch(fetchAlbumsRequest());

  }
}
