import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { TrackHistory } from '../../models/trackHistory.model';
import { deleteTrackHistoryRequest, fetchTrackHistoriesRequest } from '../../store/trackHistory.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-track-history',
  templateUrl: './track-history.component.html',
  styleUrls: ['./track-history.component.sass']
})
export class TrackHistoryComponent implements OnInit {
  trackHistories: Observable<TrackHistory[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  deleteTrackHistory: Observable<TrackHistory | null>

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.trackHistories = store.select( state => state.trackHistories.trackHistories);
    this.loading = store.select(state => state.trackHistories.fetchLoading);
    this.error = store.select(state => state.trackHistories.fetchError);
    this.deleteTrackHistory = store.select(state => state.trackHistories.trackHistory)
  }

  ngOnInit(): void {
    this.store.dispatch(fetchTrackHistoriesRequest());
  }


  onDelete(id: string) {
    this.store.dispatch(deleteTrackHistoryRequest({id: id}));
  }
}
