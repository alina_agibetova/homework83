import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchAlbumRequest, fetchAlbumsRequest, fetchArtistsRequest } from '../../store/music.actions';
import { Album } from '../../models/album.model';
import { TrackData } from '../../models/track.model';
import { createTrackRequest } from '../../store/tracks.actions';
import { Artist } from '../../models/artist.model';

@Component({
  selector: 'app-new-track',
  templateUrl: './new-track.component.html',
  styleUrls: ['./new-track.component.sass']
})
export class NewTrackComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  albums: Observable<Album[]>;
  artists: Observable<Artist[]>;

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.tracks.createLoading);
    this.error = store.select(state => state.tracks.createError);
    this.albums = store.select(state => state.albums.albums);
    this.artists = store.select(state => state.artists.artists)
  }

  ngOnInit(): void {
    this.store.dispatch(fetchAlbumsRequest());
    this.store.dispatch(fetchArtistsRequest());
  }

  onSubmit() {
    const trackData: TrackData = this.form.value;
    this.store.dispatch(createTrackRequest({trackData}));
  }

  onClick(_id: string) {
    this.store.dispatch(fetchAlbumRequest({artist: _id}))
  }
}
