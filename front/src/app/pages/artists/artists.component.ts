import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Artist, ArtistPublishData } from '../../models/artist.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { createArtistPublishRequest, deleteArtistRequest, fetchArtistsRequest } from '../../store/music.actions';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.sass']
})
export class ArtistsComponent implements OnInit {
  artists: Observable<Artist[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  deleteArtist: Observable<Artist | null>;
  publishArtist: Observable<boolean>

  constructor(private store: Store<AppState>) {
    this.artists = store.select( state => state.artists.artists);
    this.loading = store.select(state => state.artists.fetchLoading);
    this.error = store.select(state => state.artists.fetchError);
    this.deleteArtist = store.select(state => state.artists.artist);
    this.publishArtist = store.select(state => state.artists.publishLoading);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest());
  }

  onPublish(id: string){
    const publish: ArtistPublishData = {
      id: id,
      isPublished: true
    }
    this.store.dispatch(createArtistPublishRequest({artistPublishData: publish}))
  }

  onDelete(id: string){
    this.store.dispatch(deleteArtistRequest({id: id}));
  }


}
