import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Artist, ArtistData } from '../../models/artist.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { createArtistRequest } from '../../store/music.actions';

@Component({
  selector: 'app-new-artist',
  templateUrl: './new-artist.component.html',
  styleUrls: ['./new-artist.component.sass']
})
export class NewArtistComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  artists: Observable<Artist[]>

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.artists.createLoading);
    this.error = store.select(state => state.artists.createError);
    this.artists = store.select(state => state.artists.artists);
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const artistData: ArtistData = this.form.value;
    this.store.dispatch(createArtistRequest({artistData}));
  }

}
