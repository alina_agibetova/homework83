import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../models/user.model';
import { Track, TrackPublishData } from '../../models/track.model';
import { fetchAlbumRequest } from '../../store/music.actions';
import { createTrackPublishRequest, deleteTrackRequest, fetchTracksRequest } from '../../store/tracks.actions';
import { createTrackHistoryRequest } from '../../store/trackHistory.actions';
import { TrackHistory } from '../../models/trackHistory.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute } from '@angular/router';
import { MusicService } from '../../services/music.service';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.sass']
})
export class TracksComponent implements OnInit {
  user: Observable<User | null>;
  tracks: Observable<Track[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  id!: string;
  newUser!: User | null;
  albumName!: string;
  userSub!: Subscription;
  trackHistory: Observable<TrackHistory[]>;
  deleteTrack: Observable<Track | null>;
  publishTrack: Observable<boolean>

  constructor(private store: Store<AppState>, private route: ActivatedRoute, private musicService: MusicService) {
    this.tracks = store.select(state => state.tracks.tracks);
    this.loading = store.select(state => state.tracks.fetchLoading);
    this.error = store.select(state => state.tracks.fetchError);
    this.trackHistory = store.select(state => state.trackHistories.trackHistories);
    this.user = store.select(state => state.users.user);
    this.deleteTrack = store.select(state => state.tracks.track);
    this.publishTrack = store.select(state => state.tracks.publishLoading)
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchAlbumRequest({artist: params['id']}));
      this.albumName = params['name'];
      this.store.dispatch(fetchTracksRequest({id: params['id']}));
    });
    this.user.subscribe(user => {
      this.newUser = <User>user;
    });
  }

  onPublish(id: string){
    const publish: TrackPublishData = {
      id: id,
      isPublished: true
    }
    this.store.dispatch(createTrackPublishRequest({trackPublishData: publish}))
  }

  addTrack(id: string) {
    this.store.dispatch(createTrackHistoryRequest({trackHistoryData: id}));
  }

  onDelete(id: string) {
    this.store.dispatch(deleteTrackRequest({id: id}))
  }
}
