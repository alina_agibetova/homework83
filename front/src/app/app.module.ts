import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ArtistsComponent } from './pages/artists/artists.component';
import { AlbumsComponent } from './pages/albums/albums.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDividerModule } from '@angular/material/divider';
import { ImagePipe } from './pipes/image.pipe';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RegisterComponent } from './pages/register/register.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ValidateIdenticalDirective } from './validate-identical.directive';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { LoginComponent } from './pages/login/login.component';
import { CenterCardComponent } from './ui/center-card/center-card.component';
import { AppStoreModule } from './app-store.module';
import { MatMenuModule } from '@angular/material/menu';
import { AlbumComponent } from './pages/albums/album/album.component';
import { TrackHistoryComponent } from './pages/track-history/track-history.component';
import { AuthInterceptor } from './auth.interceptor';
import { NewAlbumComponent } from './pages/new-album/new-album.component';
import { MatSelectModule } from '@angular/material/select';
import { NewArtistComponent } from './pages/new-artist/new-artist.component';
import { NewTrackComponent } from './pages/new-track/new-track.component';
import { TracksComponent } from './pages/tracks/tracks.component';
import { HasRolesDirective } from './directives/has-roles.directive';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { environment } from '../environments/environment';

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [{
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.fbAppId, {
    scope: 'email, public_profile'
    })
    }
  ]
}


@NgModule({
  declarations: [
    AppComponent,
    ArtistsComponent,
    AlbumsComponent,
    ImagePipe,
    RegisterComponent,
    ValidateIdenticalDirective,
    FileInputComponent,
    LoginComponent,
    CenterCardComponent,
    AlbumComponent,
    TrackHistoryComponent,
    NewAlbumComponent,
    NewArtistComponent,
    NewTrackComponent,
    TracksComponent,
    HasRolesDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    FlexLayoutModule,
    MatDividerModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    AppStoreModule,
    MatMenuModule,
    MatSelectModule,
    SocialLoginModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
