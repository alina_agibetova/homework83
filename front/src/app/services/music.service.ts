import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiArtistData, ArtistData, ArtistPublishData } from '../models/artist.model';
import { map } from 'rxjs';
import { AlbumData, AlbumPublishData, ApiAlbumData } from '../models/album.model';
import { ApiTrackData, TrackData, TrackPublishData } from '../models/track.model';
import { ApiTrackHistoryData, TrackHistory } from '../models/trackHistory.model';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(private http: HttpClient) { }

  getArtists() {
    return this.http.get<ApiArtistData[]>('http://localhost:8000/artists').pipe(
      map( response => {
        return response
      })
    )
  }

  getAlbumId(id: string){
    return this.http.get<ApiAlbumData[]>(`http://localhost:8000/albums?artist=${id}`).pipe(
    map(result => {
      return result
    })
  );
  }

  getAlbums() {
    return this.http.get<ApiAlbumData[]>('http://localhost:8000/albums').pipe(
      map( response => {
        return response

      })
    )
  }

  getTrack(id: string){
    return this.http.get<ApiTrackData[]>(`http://localhost:8000/tracks?album=${id}`).pipe(
      map(result => {
        return result
      })
    );
  }

  createArtist(artistData: ArtistData){
    const formData = new FormData();

    Object.keys(artistData).forEach(key => {
      if (artistData[key] !== null){
        formData.append(key, artistData[key]);
      }
    });

    return this.http.post('http://localhost:8000/artists', formData);
  }

  createAlbum(albumData: AlbumData){
    const formData = new FormData();

    Object.keys(albumData).forEach(key => {
      if (albumData[key] !== null){
        formData.append(key, albumData[key]);
      }
    });

    return this.http.post('http://localhost:8000/albums', formData);
  }

  createTrack(trackData: TrackData){
    return this.http.post<ApiTrackData>('http://localhost:8000/tracks', trackData);
  }

  createTrackHistory(track: string){
    return this.http.post<ApiTrackHistoryData>('http://localhost:8000/track_history', {track})

  }

  getTrackHistory(){
    return this.http.get<ApiTrackHistoryData[]>('http://localhost:8000/track_history', ).pipe(
      map(result => {
        return result.map(trackHistoryData => {
          return new TrackHistory(
            trackHistoryData._id,
            trackHistoryData.user,
            trackHistoryData.track,
            trackHistoryData.datetime
          )
        })
      })
    );
  }

  albumIsPublish(albumPublishData: AlbumPublishData){
    const publish = {
      isPublished: true
    }
    return this.http.post<AlbumPublishData>(`http://localhost:8000/albums/${albumPublishData.id}/publish`, publish)
  }

  artistIsPublish(artistPublishData: AlbumPublishData){
    const publish = {
      isPublished: true
    }
    return this.http.post<ArtistPublishData>(`http://localhost:8000/artists/${artistPublishData.id}/publish`, publish)
  }

  trackIsPublish(trackPublishData: TrackPublishData){
    const publish = {
      isPublished: true
    }
    return this.http.post<TrackPublishData>(`http://localhost:8000/tracks/${trackPublishData.id}/publish`, publish)
  }

  deleteAlbum(id: string){
    return this.http.delete<ApiAlbumData>(`http://localhost:8000/albums/${id}`);
  }

  deleteTrackHistory(id: string){
    return this.http.delete<ApiTrackHistoryData>(`http://localhost:8000/track_history/${id}`);
  }

  deleteArtist(id: string){
    return this.http.delete<ApiArtistData>(`http://localhost:8000/artists/${id}`);
  }

  deleteTrack(id: string){
    return this.http.delete<ApiTrackData>(`http://localhost:8000/tracks/${id}`);
  }

}


